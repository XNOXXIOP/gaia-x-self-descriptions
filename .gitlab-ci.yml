include:
  - template: 'Workflows/MergeRequest-Pipelines.gitlab-ci.yml'

stages:
- unit-test # for the component without dependency
- build # build image
- test # cross component
- deploy # deploy
- release # package and publish a release

unit-test:rdf:
  stage: unit-test
  image: python:3-buster
  before_script:
  - cd ci
  - pip install -Ur requirements.txt
  script:
  - mkdir ${CI_PROJECT_DIR}/output/
  - ./check_shacl.py 2> ${CI_PROJECT_DIR}/output/check_shacl_report.json
  artifacts:
    paths:
    - output/

filter-license:
  stage: unit-test
  image: alpine:3
  before_script:
  - apk add git
  script:
  - mkdir licensed
  - >
    cat ci/LICENSE_ACCEPTED_BY.txt | egrep -v '^#|^$' | cut -d: -f2 > valid_authors.txt # extract email
  - >
    for filepath in $(find implementation/instances/ -type f); do
      mkdir -p $(dirname licensed/${filepath})
      echo ${filepath}
      git log --format='%aE' ${filepath} | sort | uniq | grep -vf valid_authors.txt || cp ${filepath} licensed/${filepath}
    done
  artifacts:
    paths:
    - licensed/

convert:ttl:
  stage: build
  image: node:15-buster # do not use alpine version because busybox find doesn't implement -printf option
  before_script:
  - npm install @frogcat/ttl2jsonld
  script:
  - rm -rf implementation/instances/* && cp -r licensed/* .
  - >
    for ttlfile in $(find implementation/instances/ -name '*.ttl' -printf '%P\n'); do
      jsonfile=${ttlfile%.ttl}.jsonld
      mkdir -p $(dirname output/${jsonfile})
      echo ${ttlfile}
      $(npm bin)/ttl2jsonld implementation/instances/${ttlfile} > output/${jsonfile} || true
    done
  dependencies:
  - filter-license
  artifacts:
    paths:
    - output/

# python -c 'import sys, yaml, json; print(yaml.dump(json.loads(sys.stdin.read())))'

convert:yamlld:
  stage: build
  image: python:3-buster
  before_script:
  - pip install -U PyYAML==5.3.1
  script:
  - rm -rf implementation/instances/* && cp -r licensed/* .
  - >
    for yamlfile in $(find implementation/instances/ -name '*.yamlld' -printf '%P\n'); do
      jsonfile=${yamlfile%.yamlld}.jsonld
      mkdir -p $(dirname output/${jsonfile})
      echo ${yamlfile}
      cat implementation/instances/${yamlfile} | python -c 'import sys, yaml, json; print(json.dumps(yaml.safe_load(sys.stdin.read())))' > output/${jsonfile}
    done
  dependencies:
  - filter-license
  artifacts:
    paths:
    - output/

copy:jsonld:
  stage: build
  image: debian:buster
  script:
  - rm -rf implementation/instances/* && cp -r licensed/* .
  - >
    for jsonfile in $(find implementation/instances/ -name '*.jsonld' -printf '%P\n'); do
      mkdir -p $(dirname output/${jsonfile})
      echo ${jsonfile}
      cp implementation/instances/${jsonfile} output/${jsonfile}
    done
  dependencies:
  - filter-license
  artifacts:
    paths:
    - output/

deploy:jsonld:
  stage: deploy
  image: python:3-buster
  variables:
    OS_REGION_NAME: GRA
    OS_AUTH_URL: https://auth.cloud.ovh.net/v3/
    OS_PROJECT_DOMAIN_NAME: Default
    OS_TENANT_NAME: 5587205303790442
    OS_USERNAME: user-3pjRdGwtmJRU
    OS_TENANT_ID: 6abcbbbfcb8d4f0fa5a7b4235070ab64
    OS_USER_DOMAIN_NAME: Default
    OS_IDENTITY_API_VERSION: 3
  before_script:
  - pip install -U python-swiftclient==3.10.1
  - pip install -U python-keystoneclient==4.1.1
  - apt-get update
  - apt-get install attr
  script:
  - swift post -r '.r:*' self-descriptions # enable reading, disable listing (default: .r:*,.rlistings)
  - >
    for jsonfile in $(find output/ -name '*.jsonld'); do
      setfattr -n user.mime_type -v "application/ld+json" ${jsonfile}
    done
  - tar --create --verbose --bzip2 --file output.tar.bz2 --directory output .
  - eval $(swift auth)
  - >
    curl -i -H "X-Auth-Token:${OS_AUTH_TOKEN}" -H "Accept: application/json" -X PUT ${OS_STORAGE_URL}/self-descriptions/${CI_COMMIT_REF_SLUG}/?extract-archive=tar.bz2 --data-binary @output.tar.bz2
  - find output -name '*.jsonld'  -printf '%P\n' | swift upload self-descriptions - --object-name ${CI_COMMIT_REF_SLUG}/files.txt
  dependencies:
  - convert:ttl
  - convert:yamlld
  - copy:jsonld
  only:
  - master
  - staging
  - ci

pages:
  stage: deploy
  image: alpine:3
  script:
  - mkdir -p public/
  - touch public/index.html
  - mkdir -p public/provider/
  - cp implementation/instances/provider/*.ttl public/provider/ 2>/dev/null || true
  - cp implementation/instances/provider/*.jsonld public/provider/ 2>/dev/null || true
  - mkdir -p public/service/
  - cp implementation/instances/service/*.ttl public/service/ 2>/dev/null || true
  - cp implementation/instances/service/*.jsonld public/service/ 2>/dev/null || true
  - cd public && find . -name \*.ttl -o -name \*.jsonld > files.txt
  artifacts:
    paths:
    - public # do not change it https://gitlab.com/gitlab-org/gitlab/-/issues/1719#note_30008931
  only:
  - master
  - staging
  
release:rdf:
  stage: release
  image: alpine:3
  before_script:
  - apk add zip curl
  - cd implementation/instances
  script:
  - zip -r provider.zip provider
  - 'curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file provider.zip ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/rdf/${CI_COMMIT_TAG}/provider.zip'
  only:
  - tags
