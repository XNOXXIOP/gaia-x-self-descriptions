# GAIA-X InfoModel


- [Das Projekt GAIA-X](https://www.bmwi.de/Redaktion/DE/Publikationen/Digitale-Welt/das-projekt-gaia-x.pdf?__blob=publicationFile&v=22)
- [tech, 1](https://www.bmwi.de/Redaktion/EN/Publikationen/gaia-x-technical-architecture.pdf)

## [Ontology](./Ontology.ttl)

The ontology serves the following purposes:

- authoritative machine-comprehensible implementation of glossary
- vocabulary/schema self-descriptions (terminology plus validation, e.g., required properties)
- inference rules (e.g., inheritable properties)


### How to aggregate the ontology into one file

The ontology is separated into several files. Therefore, it is important to keep the file `Ontology.ttl` up to date, meaning it always imports all other modules. In order to generate one ontology file containing every information follow these steps:

1) Install [Protégé](https://protege.stanford.edu/)
2) Load the file Ontology.ttl in Protégé. Make sure to have all other modules available in the specified folders. Protégé then automatically imports all other modules as specified in the file Ontology.ttl
3) Now navigate in Protégé to: File -> Save as and determine the file path.
4) The saved file contains all modules in one file
