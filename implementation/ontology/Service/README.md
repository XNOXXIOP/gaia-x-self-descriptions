
## TODO


### Coordinate creation of self descriptions for all services involved in the technical scenario 1

Currently, five services are involved in the technical scenario:

- AI4BD ML service -> self description will be created by AI4BD
- orchestration service
- ML execution service
- storage service
- monitoring service

We need self descriptions for all of these services.

seeAlso: <https://gitlab.com/gaia-x/gaia-x-core/gaia-x-community-implementation/-/issues/7#note_384441813>

---

## WIP

---

## parking lot

---

## DONE

---