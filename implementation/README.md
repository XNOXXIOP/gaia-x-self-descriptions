# Implementation

This directory contains the actual implementation of GAIA-X Self-Descriptions (SDs), including
- the ontology containing the required vocabulary terms,
- shapes+rules that define a minimum set of terms as well as data structure,
- queries that enable us to search/filter an SD repository, and
- concrete instances with/from use-case partners - including examples.

## Lesson learned and how to contribute
Whoever is ready and willing to self-describe things, do not wait for "the ontology" to be provided.
We need to be aware that for most of what we would like to describe, ontologies do not exist or only exist partly.
Thus, please write down what you would like to write down, submit a merge request and it will be reviewed/discussed/aligned/improved.
A good place for discussions on this would the the `requirements` directory, or a respective sub-directory.
