# GAIA-X Self Descriptions

This repository contains documentation, implementation, and templates on GAIA-X Self Descriptions (SDs).
For an overview, please inspect the `documentation` directory.
We collect all inputs and requirements in any form in the `input+requirements` directory.
In `implementation`, we present and develop all SD-related vocabularies, templates, examples, shapes/rules, and queries.

## Licensing

In order to get your self-descriptions files published, you must follow the steps described in [`ci/LICENSE_ACCEPTED_BY.txt`](ci/LICENSE_ACCEPTED_BY.txt)

## How to contribute

Depending on your role (cf. [role overview](/documentation/roles-overview.md), you can contribute in different ways.
- SD suppliers can supply `Provider` SDs as follows:
  - Based on the [Provider Template](/implementation/instances/provider/ProviderExample.ttl), create a copy for you, and adapt/remove/add properties as needed.
    1) Navigate to the respective template ([Provider](/implementation/instances/provider/ProviderExample.ttl) or [Service](/implementation/instances/service/ServiceExample.ttl)).
    2) Click "Web IDE" on the top right.
    3) Copy all content from that template.
    4) Create a new file "<your-name>.ttl" (via the three dots on the directory on the left).
    5) Paste the template content there and replace it with your real-world details where possible. 
      - **PLEASE NOTE: ALL SD CONTENTS MIGHT BE SHARED PUBLICLY - DO NOT ADD ANY PRIVACY-CRITICAL OR CONFIDENTIAL DETAILS**
    6) When you are done: Click "commit" on the bottom left, enter a branch name and make sure to check "start a new merge request", and save that merge request.
- SD developers will mainly work in the [implementation](/implementation) directory.
- Advisers will mainly contribute through the weekly calls as well as through advises on issues, commits, and merge requests.
